const path = require(`path`);
const { createFilePath } = require(`gatsby-source-filesystem`);

const makeRequest = (graphql, request) =>
  new Promise((resolve, reject) => {
    resolve(
      graphql(request).then(result =>
        result.errors ? reject(result.errors) : result
      )
    );
  });

const generatePages = (result, actions, template) => {
  const pages = result.data.allMarkdownRemark.edges;
  const pageTemplate = path.resolve(template);
  const { createPage } = actions;

  pages.forEach((page, index) => {
    const slug = page.node.fields.slug;
    const previous = index === pages.length - 1 ? null : pages[index + 1].node;
    const next = index === 0 ? null : pages[index - 1].node;

    createPage({
      path: `${slug}`,
      component: pageTemplate,
      context: {
        slug,
        previous,
        next,
      },
    });
  });
};

exports.createPages = ({ graphql, actions }) => {
  const getBlogPosts = makeRequest(
    graphql,
    `
      {
        allMarkdownRemark(
          filter: {frontmatter: {type: {eq: "blog"}}}
          sort: { fields: [frontmatter___date], order: DESC }
          limit: 1000
        ) {
          edges {
            node {
              fields {
                slug
              }
              frontmatter {
                title
              }
            }
          }
        }
      }
    `
  ).then(result =>
    generatePages(result, actions, `./src/templates/blog-post.js`)
  );

  const getProjects = makeRequest(
    graphql,
    `
      {
        allMarkdownRemark(
          filter: {frontmatter: {type: {eq: "project"}}}
          sort: { fields: [frontmatter___date], order: DESC }
          limit: 1000
        ) {
          edges {
            node {
              fields {
                slug
              }
              frontmatter {
                title
                type
              }
            }
          }
        }
      }
    `
  ).then(result =>
    generatePages(result, actions, `./src/templates/project.js`)
  );

  return Promise.all([getBlogPosts, getProjects]);
};

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions;

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode });
    const type = node.frontmatter.type;
    createNodeField({
      name: `slug`,
      node,
      value: `/${type}${value}`,
    });
  }
};
