module.exports = {
  trailingComma: 'es5',
  tabWidth: 2,
  singleQuote: true,
  overrides: [
    {
      files: '*.md',
      options: {
        singleQuote: false,
        printWidth: 100,
        proseWrap: 'always',
      },
    },
  ],
};
