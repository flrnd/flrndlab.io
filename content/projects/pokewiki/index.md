---
title: Pokewiki
description: GraphQL Server consuming pokeapi.co
category: Back-end development
url: https://github.com/flrnd/pokewiki
date: "2019-09-19"
keywords: "GraphQL, Nodejs, Jest, API, Apollo-server, Javascript"
type: project
---
