---
title: Candid.vim
description: A dark color scheme with vibrant colors for vim and neovim.
category: Development
url: https://github.com/flrnd/candid.vim
date: "2019-06-05"
keywords: "colors scheme, vimscript, neovim"
type: project
---
