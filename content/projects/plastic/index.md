---
title: Plastic.vim
description: Color scheme port from Plastic VSCode theme.
category: Development
url: https://github.com/flrnd/plastic.vim
date: "2019-05-11"
keywords: "colors scheme, vimscript, neovim"
type: project
---
