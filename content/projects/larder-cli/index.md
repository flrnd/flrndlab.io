---
title: Larder-cli
description: Third party client for Larder.io written in Go
category: Development
url: https://gitlab.com/flrnd/larder-cli
date: "2019-02-04"
keywords: "Go, CLI, API"
type: project
---
