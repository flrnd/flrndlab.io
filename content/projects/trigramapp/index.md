---
title: trigram app. Web and mobile Apps.
description:
  trigramapp.com is an I Ching mobile app. Both, the web app and the mobile were developed using
  modern stacks.
category: full-stack
url: https://trigramapp.com
date: "2020-09-14"
keywords: "full-stack, react, nextjs, aws, lambda, dynamodb, Adroid, IOS"
type: project
---
