---
title: florianrand.com
description: This site, build with GatsbyJS and Styled components.
category: Front-end development
url: https://gitlab.com/flrnd/flrnd.gitlab.io
date: "2019-04-06"
keywords: "JAM Stack, GatsbyJS, Styled Components, React"
type: project
---
