---
title: Fedora Linux Workstation ansible playbook
description: I started using ansible few years ago because I hate repeating some tasks every time I have to setup a new Workstation.
category: DevOps
url: https://github.com/flrnd/ansible
date: "2018-07-12"
keywords: "Ansible, DevOps, Linux"
type: project
---
