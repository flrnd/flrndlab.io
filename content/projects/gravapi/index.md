---
title: Gravapi
description: CLI tool to create API end points to convert email address into gravatar.
category: Back-end development
url: https://github.com/flrnd/gravapi
date: "2019-09-16"
keywords: "Nodejs, Express, Jest, API, Rollup, CLI, Javascript"
type: project
---
