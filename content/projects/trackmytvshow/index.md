---
title: Track my TV Show
description: RESTFull API to keep track of your favorite tv shows
category: Back-end development
url: https://github.com/flrnd/trackmytvshows
date: "2019-11-27"
keywords: "Nodejs, Express, MongoDB, Jest, API, Typescript, Winston, Morgan"
type: project
---
