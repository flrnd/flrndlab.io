---
title: From Graphic Design to Development
date: "2019-04-11T15:32:32"
description:
  "A post about how and why I started my late career transition from Design to Development"
keywords: late career change,graphic designer, Development,blog
coverImage: ./scott-rodgerson-z0MDyylvY1k-unsplash90.jpg
type: blog
---

I don't know how many of you have changed careers at 40 (years old, not Km per hour).

It's not only scary but also hard. Every morning and I really mean every f. morning I wake up with
the same thoughts. But before to start with that let me explain how I ended here in the first place.

My background. I am nobody. I don't have a degree with excellent grades, nor I became a billionaire
at 12 with my first business.

For family reasons I had to start working before finishing my studies, but I'm very grateful for
everything I've accomplished. I am a self-taught, simple guy, who had survived all along.

During the past 18 years, I've worked as a waiter, cook, photographer, retoucher, CGI artist,
graphic designer, art director, web designer and some fron-end development, for both small and big advertising agencies, with some
freelancing in between. I've even worked for some big brands, in the automobile industry.

None of this did matter in the past three years when my mother got really really sick, and I had to
choose between helping her or my career. Obviously, I had to help her. I moved back to my old town.
A small city with **fewer job opportunities than a swimwear store in the desert**.

For two years I was divided between looking for clients and hospitals because of my mother's
illness. I failed miserably my attempt to get new clients. Do you know what a freelancer without
clients is? A factory of absurd and disparate ideas. But above that, it's a target for predators. I
was so desperate to get clients that instead only got worst gigs ever in my entire career.

One summer, started my vacations and at the end of the month (and not being paid for
two months of freelancing work), I was so exhausted that I couldn't stand the situation anymore. 
Thanks to not having wasted money in these years I had enough savings to stop freelancing. 
I really needed some time off. Here, my penultimate post on Instagram as a Graphic Design Studio.

![Disparate composition that convey my crazy need to go on vacations](./instagram-brdt.jpg "My penultimate post on instagram as graphic Design
Studio")

A few weeks after and a lot of thinking I realized that all these years I was valued not only for my
graphics skills, or my taste for typography or branding knowledge (probably the image above is not a
good reference) but because I was good with computers too. For example back when I worked as a CGI generalist,
I helped the sysadmin to set up the render farm. And even more, I set another render cluster
using the office workstations, 24 beautiful Supermicro machines ready to be uses as a backup for
fast rendering at lunch or during the nights when the main render queue was collapsed with jobs.

I installed my first Linux distribution back in 1996. Since then, I always had some kind of attraction to the command line.
Even working as a designer, there was times when I was looking for excuses to do some coding, like a very simple web app to render some 3D models from work  using WebGL, 
or a python script to scrap some data and avoid downloading hundreds of excels one by one.

Holding that thought, that summer started installing WSL (Windows Subsystem for Linux) on my Windows machine, and a few weeks after, I was installing a full linux distro in a new SSD disk. That feeling of an old friend reunion, as if those last 6 years that I spent in the design industry were just not my path, not exactly as a 'wasting time' feeling but more of a detour, and coming back on track.

I love photography and design, and I admit I have some kind of thing for arts and everything visual, but whatever I do,
I always end comming back to a blinking cursor in a dark terminal.

Cover image by [_Scott Rodgerson @unsplash_](https://unsplash.com/@scottrodgerson)
