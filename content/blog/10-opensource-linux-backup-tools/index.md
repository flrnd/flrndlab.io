---
title: 10 opensource Linux Backup tools
date: "2009-09-03T14:03:00"
description: "Opensource linux backup tools"
keywords: Linux,Opensource,Backup,tools
coverImage: ./markus-spiske-hL8slYnc-bM-unsplash.jpg
type: blog
---

Making backup copies of our system is an important task that many users forget just until a disaster
occurs. For that same reason, I have selected 10 simple-to-use Linux backup applications.

The vast majority of these mimic the incremental system used by Apple's Time Machine, creating a
complete backup of what we want to save and then saving backup copies of the changes made since the
last copy, allowing you to restore a file or several selecting a point in time. The only two
applications in this list that differ are CloneZilla and Mondo Rescue. CloneZilla is a clone of the
famous Norton Ghost and its main function is to create completely identical images of a disk or
partition. On the other hand, Mondo Rescue is designed as a utility for disaster recovery, creating
complete images of the entire system. For more information you just have to follow the links and
look at the documentation of each one, they really do not have many complications when setting up
and other stories, which is why I have saved the descriptions and characteristics of each one.


Finally, I would like to clarify that the order of the list has been completely involuntary, so the
quality of the applications are not related to the position in which they appear.


* [Areca Backup](http://www.areca-backup.org/)
* [Backerupper](https://backerupper.sourceforge.net)
* [Back in time](https://github.com/bit-team/backintime)
* [Lucky backup](http://luckybackup.sourceforge.net/)
* [Simple Backup  Solution](https://sourceforge.net/projects/sbackup/)
* [Flyback](https://code.google.com/archive/p/flyback/)
* [Time vault](https://wiki.ubuntu.com/TimeVault)
* [Déjà Dup](https://wiki.gnome.org/Apps/DejaDup)
* [Clonezilla](https://clonezilla.org/)
* [Mondo Rescue](http://www.mondorescue.org/)

Cover image by [_Markus Spiske @unsplash_](https://unsplash.com/@markusspiske)
