---
title: Hello World
date: "2019-04-10T15:51:00"
description: "The first post on my personal gatsby blog"
keywords: Blog,Gatsbyjs,flrnprz,developer,Hello world
coverImage: ./karine-germain-573941-unsplash.jpg
type: blog
---

There is no blog without Hello World post.

Be aware this site is a _work in progress_ project, mainly used as a testing playground with gatsby
and react.

Cover image by [_Karine Germain @unsplash_](https://unsplash.com/@lillooette)
