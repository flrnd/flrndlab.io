---
title: How I stay productive working at home
date: "2019-04-29T15:51:00"
description: "These are the key points that help me stay productive working remotely"
keywords: productivity,career,development,change
coverImage: ./nathan-riley-514162-unsplash.jpg
type: blog
---

One of the challenges (among others) we face working at home is being productive. In terms of
productivity, not everything works with everyone. I've learned the hard way few lessons in the past
few years that I want to share.

Some of them may appear silly but believe me that these changes were a huge step forward in my
productivity.

## It is important to get dressed every morning

As I said, maybe it seems silly, but it's easy to start neglecting everyday activities, for example,
is tempting being all day in pajamas if I don't have to go out.

That's why I wake early every morning, take a shower, have my breakfast and then go for a morning
walk before sitting down in my desk. This routine helps me a lot.

## A clean and comfortable working space is a must

This includes things like:

- Comfortable and ergonomic chair
- Clean desk with only necessary things: laptop, notebook, and pen.
- Natural light.
- And silence.

It took me a while to realize how my productivity improved in a clean and diaphanous environment.
Think about it, productivity is mostly about getting ~~shit~~ things done. Since we have the
opportunity to prepare a perfect office at home, why wouldn't invest some time (and money) in
configuring a perfect working place? In fact, a simple google search _best offices around the world_
can show how important it is.

During these years, not every time I had a proper environment. I've lived in places that didn't pass
all the checklist. It took a lot of frustration to realize that there are other alternatives, like a
Coworking.

## Organizing daily tasks

There are a lot of ways of achieving this. From tools to methodologies. I've tried more productivity
tools than I'm ready to admit.

At the end of the day, a hand-made Kanban board, post-ist, and a few notebooks do the job for me.
After all, I'm a designer. I love notebooks, scribbling, and sketching.

Tools or techniques are not that important. The important key here is to maintain a level of
organization using whatever works the best for you.

## Don't get stuck with the same task for too long

This is very simple. If you find yourself doing something that is taking too long (unless you don't
have anything else to do) stop that and try to finish those boring and effortless activities that
you have to do anyway. Then you can approach with a fresh mind that problem next day, and you'll be
happy because that boring stuff is crossed from your list.

## Force yourself to disconnect

Working at home, it's pretty easy falling into the _always working trap_. If you work at home, you
know what I'm talking about.

We need to rest to be productive. That's why I organize my daily routine with fixed working hours.
Sometimes, I work more hours than it was intended for that day, but there is always a good reason
for that.

The rest of the time, when I finish my daily work, I continue with my life. I force myself to go
outside although sometimes a good book it's enough to enjoy my free time.

And mostly this is it for me. Do you have any special needs to stay productive?

Cover image by [_Nathan Riley @unsplash_](https://unsplash.com/@nrly)
