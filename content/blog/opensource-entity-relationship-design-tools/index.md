---
title: Open Source Entity-Relationship model design tools
date: "2008-10-17T17:44:22"
description: Open source Entity-Relationship model design tools
keywords: Opensource,Linux,DBM,database,entity-relationship,diagram
coverImage: ./ER_Diagram_MMORPG.webp
type: blog
---

I found recently [DB Designer Fork](https://sourceforge.net/projects/dbdesigner-fork/) and as its
name implies, is a fork of [DB Designer](https://www.dbdesigner.net/). DB Designer is primarily
intended to work with MySQL.

There are a lot of applications for modelling, from diagrams in UML to even applications that take
you out of the design by reverse engineering your database.

List of the most viable Open Source options:

- [MySQL Workbench](https://www.mysql.com/products/workbench/)
- ~~[DDT (database design tool)](http://www.visi.com/~jjanssens/)~~
- [Open System Architect](https://www.codebydesign.com/)
- [pgDesigner](https://sourceforge.net/projects/pgdesigner/)
- [SQL Power Architect](http://www.bestofbi.com/page/architect)
