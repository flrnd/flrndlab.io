---
title: A simple react a11y Emoji component
date: "2019-05-28T13:00:00"
description: "A simple Emoji component written in react with a focus on accessibility"
keywords: react,a11y,beginner,accessibility,blog,emoji
coverImage: ./jfl-GfmIT_pseDQ-unsplash90.jpg
type: blog
---

Emojis are cool, right? Everyone ❤️ emojis, but screen readers have a different opinion about the
subject.

For those who don't already know, there are two attributes very useful to help screen readers:

`aria-label` adds a string label to an element, it's useful in cases that the text label is _not_
visible on the screen.

`aria-hidden`
([From MDN](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_aria-hidden_attribute)):

> Adding aria-hidden="true" to an element removes that element and all of its children from the
> accessibility tree. This can improve the experience for assistive technology users by hiding:
> Purely decorative content, such as icons or images. Duplicated content, such as repeated text.
> Offscreen or collapsed content, such as menus.

So, these attributes seems a very good idea to our Emoji component. Let's see it in action:

```javascript
import React from "react";

const Emoji = ({ label, symbol }) => (
  <span
    className="emoji"
    role="img"
    aria-label={label ? label : ""}
    aria-hidden={label ? "false" : "true"}
  >
    {symbol}
  </span>
);

export default Emoji;
```

If we don't pass a label prop, we want to set aria-hidden to true so screen readers ignore our
emoji.

And some use example:

```javascript
<Emoji
  label="open book"
  symbol="📖" />
  {timeToRead} min read
```

Which renders something like this:

```html
<span
  class="emoji"
  role="img"
  aria-label="open book"
  aria-hidden="false">📖</span>
2 min read
<!-- ... ->
```

And this is it! Fun isn't it? Happy ❤️☕️🐑🐧!

Cover image by [_JFL @Unsplash_](https://unsplash.com/@wallsbyjfl)
