---
title: Linux Virtual Server, Clusters in Linux
date: "2008-12-07T18:33:21"
description: "Understanding Clusters in Linux servers"
keywords: Linux,Cluster,LVS,Linux server,sysadmin
coverImage: ./taylor-vick-M5tzZtFCOfs-unsplash.jpg
type: blog
---

## What is LVS

An LVS cluster is a group of real servers that transparently for the user acts as if it were a
single large server. This homogeneous server is called "Virtual Server". Real servers are managed by
a Director (also called load balancer or load balancer). The essential part of the director is IPVS.
IPVS (IP Virtual Server) implements a transport layer within the Linux kernel that distributes
client requests among real servers (load balancing). When we talk about load balancing in networks
we have different types. All of them are based on the different layers of the
[OSI model](https://en.wikipedia.org/wiki/OSI_model), specifically, IPVS is a Layer-4 load balancer,
because it uses protocols such as UDP, TCP and SCTP to redistribute the packets over the network
(Transport layer).

At this point, we must understand that the heart of a cluster is precisely the Director. It is
crucial for the proper functioning of our cluster to ensure the availability of at least one
Director in operation. For this, we only need a machine powerful enough to take over the work of the
Director in the event of a failure of the Principal Director (or also to perform maintenance tasks).
This is possible by using applications such as [heartbeat](http://linux-ha.org/wiki/Heartbeat) or
[UltraMonkey](http://www.ultramonkey.org/). This is where we enter the cluster management part and
ensure its availability.

There is a large amount of software that, together with LVS, works for cluster management to provide
a robust service, ensuring its availability and good performance. A list (taken from the official
[Linux Virtual Server](http://www.linuxvirtualserver.org/) page) could be the following:

- [Piranha](http://www.redhat.com/software/rha/cluster/piranha/)
- [Keepalived](https://www.keepalived.org/)
- [UltraMonkey](http://www.ultramonkey.org/)
