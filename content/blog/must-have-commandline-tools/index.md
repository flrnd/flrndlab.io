---
title: Must have command-line tools!
date: "2020-01-14T12:51:21"
description: "Must have command-line tools to increase productivity"
keywords: productivity,command-line,tools
coverImage: ./chris-barbalis-9LCMig_EblU-unsplash.jpg
type: blog
---

It's been a while since my last post, and I thought it would be a nice new year start sharing my
favourite command-line tools.

Here we go, my favourite command-line tools to increase productivity:

- [bat](https://github.com/sharkdp/bat), A cat(1) clone with syntax highlighting and Git
  integration.
- [ag](https://github.com/ggreer/the_silver_searcher), The silver searcher. A code-searching tool
  similar to ack, but faster.
- [ripgrep](https://github.com/BurntSushi/ripgrep), another alternative to ack and ag.
- [fd](https://github.com/sharkdp/fd), A simple, fast and user-friendly alternative to 'find'.
- [fzf](https://github.com/junegunn/fzf), A command-line fuzzy finder.
- [forgit](https://github.com/wfxr/forgit), Utility tool powered by fzf for using git interactively.
- [ranger](https://github.com/ranger/ranger), A VIM-inspired file manager for the console.
- [tig](https://github.com/jonas/tig), Text-mode interface for git.
- [hub](https://github.com/github/hub), A command-line tool that makes git easier to use with
  GitHub.
- [lab](https://github.com/lighttiger2505/lab), like hub but for Gitlab.
- [httpie](https://github.com/jakubroztocil/httpie), Modern command line HTTP client – user-friendly
  curl alternative with intuitive UI, JSON support, syntax highlighting, wget-like downloads,
  extensions, etc.
- [jq](https://stedolan.github.io/jq/), A lightweight and flexible command-line JSON processor.
- [exa](https://github.com/ogham/exa), A modern version of ‘ls’.
- [broot](https://github.com/Canop/broot), A new way to see and navigate directory trees.

Cover image by [_Chris Barbalis @unsplash_](https://unsplash.com/@cbarbalis)
