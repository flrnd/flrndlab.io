const seoPages = {
  index: {
    seoTitle: 'Web design & Software development',
    seoDescription:
      'Florian Rand is a multidisciplinary designer and software developer passionate with technology and design.',
    seoKeywords: [
      'Design',
      'Designer',
      'Developer',
      'Gatsbyjs',
      'Software development',
      'nodejs',
      'React',
      'React developer',
      'nodejs developer',
    ],
  },
  blog: {
    seoTitle: 'Articles about software development and design',
    seoDescription:
      'Florian Rand lastest articles about software development, design, Linux and Open Source.',
    seoKeywords: [
      'Carrer change',
      'blog',
      'Personal site',
      'Personal blog',
      'Design',
      'Designer',
      'Developer',
      'Gatsbyjs',
      'gitlab',
      'Software development',
      'Javascript',
      'nodejs',
      'React',
      'React developer',
      'nodejs developer',
      'golang developer',
      'Development blog',
      'Design blog',
    ],
  },
  about: {
    seoTitle: 'How I became a designer and a software developer.',
    seoDescription:
      'Take a look into my biography to understand my passion and why I am a designer and developer.',
    seoKeywords: [
      'designer',
      'design',
      'development',
      'developer',
      'technology',
      'art',
      'linux',
      'Open Source',
      'React developer',
      'Gatsbyjs',
      'gatsbyjs developer',
    ],
  },
  projects: {
    seoTitle: 'Software development and design porfolio',
    seoDescription: 'Take a look into a curated selection of projects.',
    seoKeywords: [
      'nodejs',
      'javascript',
      'design',
      'web design',
      'github project',
      'open source',
    ],
  },
};

export default seoPages;
