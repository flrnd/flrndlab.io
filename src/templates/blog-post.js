import React from 'react';
import { Link, graphql } from 'gatsby';
import Img from 'gatsby-image';
import Layout from '../components/layout';
import { Container } from '../components/design/layout';
import SEO from '../components/seo';
import Emoji from '../components/emoji';
import BackToTop from '../components/back-to-top';
import { FaArrowLeft, FaArrowRight } from 'react-icons/fa';

const BlogPostTemplate = props => {
  const post = props.data.markdownRemark;
  const siteTitle = props.data.site.siteMetadata.title;
  const { previous, next } = props.pageContext;
  const { title, coverImage, date } = post.frontmatter;
  const keywords = post.frontmatter.keywords.split(',');
  const location = props.location;
  const { excerpt, timeToRead } = post;
  const coverImageSizes = coverImage.childImageSharp.sizes;

  const bottomNav = (
    <div
      className="d-flex justify-content-between"
      style={{ fontSize: '0.90em', padding: '1.5em 0 1.5em 0' }}
    >
      <span className="d-flex justify-content-start align-left">
        {previous && (
          <Link
            to={previous.fields.slug}
            rel="prev"
            style={{ borderBottom: 'none' }}
          >
            <span>
              <FaArrowLeft /> {previous.frontmatter.title}
            </span>
          </Link>
        )}
      </span>
      <span className="d-flex justify-content-end align-right">
        {next && (
          <Link
            to={next.fields.slug}
            rel="next"
            style={{ borderBottom: 'none' }}
          >
            <span>
              {next.frontmatter.title} <FaArrowRight />
            </span>{' '}
          </Link>
        )}
      </span>
    </div>
  );

  return (
    <Layout location={location} title={siteTitle}>
      <SEO seoTitle={title} seoDescription={excerpt} seoKeywords={keywords} />
      <Container className="text-content">
        <article>
          <Img sizes={coverImageSizes} fadeIn={true} />
          <h1>{title}</h1>
          <div className="sub-heading">
            {date} <Emoji label="open book" symbol="📖" /> {timeToRead} min read
          </div>
          <section dangerouslySetInnerHTML={{ __html: post.html }} />
        </article>
        {bottomNav}
        <BackToTop />
      </Container>
    </Layout>
  );
};

export default BlogPostTemplate;

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
        author
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 160)
      html
      timeToRead
      frontmatter {
        title
        date(formatString: "MMMM DD, YYYY")
        keywords
        description
        coverImage {
          childImageSharp {
            sizes(maxWidth: 1280) {
              ...GatsbyImageSharpSizes
            }
          }
        }
      }
    }
  }
`;
