import React from 'react';
import { Link, graphql } from 'gatsby';
import Layout from '../components/layout';
import { Container } from '../components/design/layout';
import SEO from '../components/seo';
import BackToTop from '../components/back-to-top';
import { FaArrowLeft, FaArrowRight } from 'react-icons/fa';

const ProjectsTemplate = props => {
  const project = props.data.markdownRemark;
  const siteTitle = props.data.site.siteMetadata.title;
  const { previous, next } = props.pageContext;
  const { title, date, category, client, url } = project.frontmatter;
  const keywords = project.frontmatter.keywords.split(',');
  const location = props.location;
  const { excerpt } = project;

  const bottomNav = (
    <div
      className="d-flex justify-content-between"
      style={{ fontSize: '0.90em', padding: '1.5em 0 1.5em 0' }}
    >
      <span className="d-flex justify-content-start align-left">
        {previous && (
          <Link
            to={previous.fields.slug}
            rel="prev"
            style={{ borderBottom: 'none' }}
          >
            <span>
              <FaArrowLeft /> {previous.frontmatter.title}
            </span>
          </Link>
        )}
      </span>
      <span className="d-flex justify-content-end align-right">
        {next && (
          <Link
            to={next.fields.slug}
            rel="next"
            style={{ borderBottom: 'none' }}
          >
            <span>
              {next.frontmatter.title} <FaArrowRight />
            </span>{' '}
          </Link>
        )}
      </span>
    </div>
  );

  return (
    <Layout location={location} title={siteTitle}>
      <SEO seoTitle={title} seoDescription={excerpt} seoKeywords={keywords} />
      <Container>
        <article>
          <h1>{title}</h1>
          <section>
            <strong>Client:</strong> {client}
          </section>
          <secion>
            <strong>URL:</strong> {url}
          </secion>
          <section>
            <strong>From:</strong> {date}
          </section>
          <section>
            <strong>Category:</strong> {category}
          </section>
          <section dangerouslySetInnerHTML={{ __html: project.html }} />
        </article>
        {bottomNav}
        <BackToTop />
      </Container>
    </Layout>
  );
};

export default ProjectsTemplate;

export const pageQuery = graphql`
  query ProjectBySlug($slug: String!) {
    site {
      siteMetadata {
        title
        author
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 160)
      html
      timeToRead
      frontmatter {
        title
        category
        url
        date(formatString: "MMMM DD, YYYY")
        keywords
        description
      }
    }
  }
`;
