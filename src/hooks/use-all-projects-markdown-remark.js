import { useStaticQuery, graphql } from 'gatsby';

export const useAllProjectsMarkdownRemark = () => {
  const { allMarkdownRemark } = useStaticQuery(
    graphql`
      query AllProjects {
        allMarkdownRemark(
          sort: { fields: [frontmatter___date], order: DESC }
          filter: { frontmatter: { type: { eq: "project" } } }
        ) {
          edges {
            node {
              fields {
                slug
              }
              timeToRead
              frontmatter {
                date(formatString: "MMMM DD, YYYY")
                title
                type
                description
                category
                url
                keywords
              }
            }
          }
        }
      }
    `
  );
  return allMarkdownRemark;
};
