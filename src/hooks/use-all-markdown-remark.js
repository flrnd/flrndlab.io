import { useStaticQuery, graphql } from 'gatsby';

export const useAllMarkdownRemark = () => {
  const { allMarkdownRemark } = useStaticQuery(
    graphql`
      query AllPosts {
        allMarkdownRemark(
          sort: { fields: [frontmatter___date], order: DESC }
          filter: { frontmatter: { type: { eq: "blog" } } }
        ) {
          edges {
            node {
              fields {
                slug
              }
              timeToRead
              frontmatter {
                date(formatString: "MMMM DD, YYYY")
                title
                type
                description
                coverImage {
                  childImageSharp {
                    resize(width: 400) {
                      src
                    }
                  }
                }
              }
            }
          }
        }
      }
    `
  );
  return allMarkdownRemark;
};
