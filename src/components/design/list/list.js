import React from 'react';
import styled from 'styled-components';

export const List = styled.ul`
  display: flex;
  flex-direction: row;
  position: relative;
  list-style: none;
`;

export const ListItem = styled.li`
  text-decoration: none;
  padding-right: 0.5em;
`;

export const ListItemLink = props => {
  const { href, children, label, border } = props;
  return (
    <ListItem>
      <a href={href} style={{ borderBottom: `${border}` }} aria-label={label}>
        {children}
      </a>
    </ListItem>
  );
};
