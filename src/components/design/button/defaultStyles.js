import { palette } from '../color';

const defaultStyles = {
  primary: {
    background: palette.foregound,
    color: palette.background,
    border: palette.foregound,
  },
  secondary: {
    background: 'transparent',
    color: palette.foregound,
    border: palette.foregound,
  },
  transparent: {
    background: 'transparent',
    color: palette.foregound,
    border: 'transparent',
  },
  fontSize: {
    xl: '2.4rem',
    lg: '1.8rem',
    md: '1.3rem',
    nm: '1rem',
    sm: '0.75rem',
  },
};

export default defaultStyles;
