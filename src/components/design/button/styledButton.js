import styled from 'styled-components';
import { fonts } from '../styles/fonts';

const StyledButton = styled.button`
  display: inline-block;
  text-align: center;
  vertical-align: middle;
  box-sizing: border-box;
  cursor: pointer;
  outline: none;
  font: inherit;
  text-decoration: none;
  padding: 0.375rem 0.75rem;
  overflow: visible;
  text-transform: none;
  border-radius: 0.25rem;
  border: 3px solid ${props => props.border};
  background: ${props => props.background};
  color: ${props => props.color};
  opacity: 0.8;
  font-size: ${props => props.fontSize};
  font-family: ${fonts.primary};
  font-weight: ${props => props.fontWeight};
  transition: 0.3s;
`;

export default StyledButton;
