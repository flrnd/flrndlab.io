import React from 'react';
import StyledButton from './styledButton';
import defaultStyles from './defaultStyles';

const Button = props => {
  const {
    a11yTitle,
    type,
    color,
    fontSize,
    disabled,
    children,
    fontWeight,
  } = props;
  let colors = {};
  let isDisabled = false;
  let size;
  let weight = 400;

  color ? (colors = defaultStyles[color]) : (colors = defaultStyles['primary']);
  fontSize ? (size = fontSize) : (size = defaultStyles.fontSize.nm);

  if (disabled) {
    isDisabled = true;
    colors.color = 'black';
  }

  if (fontWeight) weight = fontWeight;

  return (
    <>
      <StyledButton
        aria-label={a11yTitle}
        type={type}
        background={colors.background}
        color={colors.color}
        border={colors.border}
        disabled={isDisabled}
        fontSize={size}
        fontWeight={weight}
      >
        {children}
      </StyledButton>
    </>
  );
};

export default Button;
