import React from 'react';
import styled from 'styled-components';
import { palette } from '../color';
import { GoCode } from 'react-icons/go';
import { fonts } from '../styles/fonts';

const StyledCard = styled.div`
  background: white;
  color: ${palette.foregound};
  border-radius: 4px;
  box-shadow: 0 2px 1px -1px rgba(0, 0, 0, 0.2), 0 1px 1px 0 rgba(0, 0, 0, 0.14),
    0 1px 3px 0 rgba(0, 0, 0, 0.12);
  min-height: 100%;
`;

const CardTitle = styled.div`
  grid-area: title;
`;

const Heading = styled.h2`
  font-size: 26px;
`;

const Category = styled.div`
  font-size: 14px;
  margin-top: -5px;
  text-transform: uppercase;
`;
const CardContainer = styled.article`
  display: grid;
  height: 90%;
  grid-template-areas:
    'title'
    'content'
    'link';
  grid-template-rows: auto 1fr auto;
  padding: 20px;
`;

const CardContent = styled.div`
  grid-area: content;
  height: 100%;
  margin: 1em 0 1em 0;
`;

const Keywords = styled.div`
  margin-top: 15px;
  font-size: 14px;
`;
const CardLink = styled.div`
  grid-area: link;
  font-family: ${fonts.secondary};
`;
const Card = ({ title, description, category, url, keywords }) => (
  <StyledCard>
    <CardContainer>
      <CardTitle>
        <Heading>{title}</Heading>
        <Category>{category}</Category>
      </CardTitle>
      <CardContent>
        {description}
        <Keywords>{keywords}</Keywords>
      </CardContent>
      <CardLink>
        <a
          aria-label="source code"
          href={url}
          style={{
            borderBottom: 'none',
            display: 'inline-flex',
          }}
        >
          <GoCode />{' '}
          <span style={{ fontSize: '16px', marginTop: '-2px' }}>Code</span>
        </a>
      </CardLink>
    </CardContainer>
  </StyledCard>
);

export default Card;
