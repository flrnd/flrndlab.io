import styled from 'styled-components';

const Footer = styled.footer`
  grid-area: footer;
  display: flex;

  align-items: center;
  justify-content: center;
  flex-direction: column;

  width: 900px;
  max-width: 90vw;

  margin-right: auto;
  margin-left: auto;

  font-size: 0.75em;

  padding-top: 1.5em;
  padding-bottom: 1em;

  overflow: hidden;
  border-top: rgb(28, 26, 115, 0.2) 1px solid;
`;

export default Footer;
