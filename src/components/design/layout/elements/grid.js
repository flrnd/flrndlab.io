import styled from 'styled-components';

const Grid = styled.div`
  display: grid;
  margin-top: 2em;
  margin-bottom: 2em;
  grid-template-columns: 1fr;
  grid-template-rows: auto;
  grid-gap: 10px;

  @media only screen and (min-width: 769px) {
    grid-template-columns: 1fr 1fr;
  }

  @media only screen and (min-width: 992px) {
    grid-template-columns: 1fr 1fr 1fr;
  }
`;
export default Grid;
