import styled from 'styled-components';
import { palette } from '../../color';

const Header = styled.header`
  grid-area: header;
  display: block;
  box-sizing: border-box;
  width: 100%;
  height: 70px;
  margin-left: auto;
  margin-right: auto;
  background-color: ${palette.background};
  border-bottom: rgb(28, 26, 115, 0.2) 1px solid;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  z-index: 1;
`;

export default Header;
