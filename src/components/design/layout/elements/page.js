import styled from 'styled-components';

const Page = styled.div`
  display: grid;
  min-height: 100vh;

  grid-template-areas:
    'header'
    'main'
    'footer';

  grid-template-rows: auto 1fr auto;
  margin: 0;
`;

export default Page;
