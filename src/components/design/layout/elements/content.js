import styled from 'styled-components';

const Content = styled.main`
  width: 100%;
  margin-top: 100px;
`;

export default Content;
