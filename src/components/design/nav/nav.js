import React from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';
import { palette } from '../color';
import { fonts } from '../styles/fonts';

export const Navbar = styled.nav`
  display: flex;
  flex-wrap: wrap;
  position: relative;
  align-items: center;
  justify-content: space-between;
  margin-top: 15px;
`;

export const Nav = styled.ul`
  display: flex;
  flex-direction: row;
  font-size: 1em;
  padding-left: 0;
  list-style: none;
  margin-left: auto;

  @media screen and (max-width: 394px) {
    font-size: 0.8em;
  }
`;

export const NavItem = styled.li`
  font-weight: ${fonts.weight.regular};
  text-transform: lowercase;
  padding: 0.4em;
`;

export const NavBrand = styled.div`
  font-weight: ${fonts.weight.bold};
  font-size: 0.9em;
  text-transform: uppercase;
  letter-spacing: 0.1em;

  @media screen and (max-width: 394px) {
    font-size: 0.6em;
  }
`;

export const NavItemLink = props => {
  const { href, children } = props;
  return (
    <NavItem>
      <Link
        to={href}
        style={{ borderBottom: 'none' }}
        activeStyle={{
          paddingBottom: '5px',
          borderBottom: `${palette.accent} 0.25em solid`,
        }}
      >
        {children}
      </Link>
    </NavItem>
  );
};
