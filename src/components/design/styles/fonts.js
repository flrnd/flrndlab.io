export const fonts = {
  primary: '"Source Sans Pro", "Helvetica Neue", Arial, sans-serif',
  secondary: '"Work Sans", "Helvetica Neue", Arial, sans-serif',
  weight: { regular: 400, bold: 700 },
};
