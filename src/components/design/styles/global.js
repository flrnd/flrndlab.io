import { createGlobalStyle } from 'styled-components';
import { normalize } from 'styled-normalize';
import { palette } from '../color';
import { fonts } from './fonts';

const GlobalStyle = createGlobalStyle`
  ${normalize}

  * { margin: 0; padding: 0; }

  ::selection{
    background-color: ${palette.accent};
    color: ${palette.foregound};
  }

  html, body {
    overflow-x: hidden;
  }

  body {
    font-family: ${fonts.secondary};
    font-weight: ${fonts.weight.regular};
    font-size: 18px;
    line-height: 1.5;
    color: ${palette.foregound};
    background-color: ${palette.background};
 }

  strong {
    font-weight: ${fonts.weight.bold}
  }

  a {
    color: ${palette.foregound};
    text-decoration: none;
    border-bottom: ${palette.accent} 0.125em solid;
  }

  a span {
    border-bottom: ${palette.accent} 0.085em solid;
  }

  }
  a:hover {
  border-bottom: ${palette.accent} 0.2em solid;
  }

  a:visited {
    color: ${palette.foregound}
  }

  .btn {
    border-bottom: none;
  }

  .btn:hover {
    border-bottom: none;
  }

  button:hover {
    opacity: 1;
  }

  h1, h2, h3, h4, h5, h6 {
    font-family: ${fonts.primary};
    font-weight: ${fonts.weight.bold};
    margin-bottom: .5rem;
    line-height: 1.2;
  }

  h1 {
    font-size: 4.5rem;

    @media (max-width: 576px) {
      font-size: 2.5rem;
    }
  }

  h2 {
    font-size: 4rem;

    @media (max-width: 576px) {
      font-size: 2rem;
    }
  }

  h3 {
    font-size: 3.75rem;

    @media (max-width: 576px) {
      font-size: 1.75rem;
    }
  }

  h4 {
    font-size: 3.5rem;

    @media (max-width: 576px) {
      font-size: 1.5rem;
    }
  }

  h5 {
    font-size: 3.25rem;

    @media (max-width: 576px) {
      font-size: 1.25rem;
    }
  }

  h6 {
    font-size: 2rem;

    @media (max-width: 576px) {
      font-size: 1.2rem;
    }
  }

  .max-width {
    margin-right: auto;
    margin-left: auto;
    width: 100%;
    max-width: 90vw;
  }

  .text-content {
    margin: 0 auto;
    max-width: 680px;
    width: 100%
  }

  .author {
    font-family: ${fonts.primary};
  }

  .heading {
    font-size: 3rem;
    margin-top: -0.2rem;
  }

  .heading span {
    border-bottom: ${palette.accent} 0.075em solid;
  }

  .heading span:hover {
    border-bottom: ${palette.accent} 0.125em solid;
  }

  .sub-heading {
    font-family: ${fonts.secondary};
    font-size: 0.75em;
    margin-bottom: 4rem;
    padding-bottom: 1em;
    text-transform: uppercase;
    border-bottom: rgb(28, 26, 115, 0.2) 1px solid;
  }

  .uppercase {
    text-transform: uppercase;
  }

  p {
    margin-top: 0;
    margin-bottom: 1rem;
  }

  blockquote {
    margin: 0 0 1rem;
  }

  .hero {
    font-weight: ${fonts.weight.bold};
    font-size: 2.5em;

    @media screen and (min-width: 756px) {
      font-size: 3em;
      line-height: 1em;
    }
  }

  .p-hero {
    font-size: 1.2em;
  }

  code {
    font-size: 0.8rem;
  }

  pre {
    font-size: 12px;
    overflow-x: scroll;
  }

  .title {
    color: transparent;
    transform: rotate(2deg)
    margin-top: 0;
    font-size: 5rem;
    text-align: left;
    -webkit-text-stroke: 2px ${palette.foregound};
    text-shadow: 3px 3px ${palette.accent};
  }
  .small {
    font-size: 2em;
  }

  .d-flex {
    display: flex;
  }

  .flex-direction-column {
    flex-direction: column;
  }
  .justify-content-between {
    justify-content: space-between;
  }

  .justify-content-start {
    justify-content: flex-start
  }

  .justify-content-center {
    justify-content: center;
  }

  .justify-content-end {
    justify-content: flex-end;
  }

  .align-right {
    text-align: right;
  }

  .align-left {
    text-align: left;
  }
`;
export default GlobalStyle;
