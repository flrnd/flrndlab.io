import React from 'react';
import { Link } from 'gatsby';
import Emoji from './emoji';
import {
  Page,
  Content,
  Header,
  Footer,
  Container,
} from '../components/design/layout';
import { Navbar, NavItemLink, NavBrand, Nav } from './design/nav';
import GlobalStyle from '../components/design/styles/global';
import ListSocialIcons from '../components/socialIcons';

const Layout = props => {
  const { title, children } = props;

  const logo = (
    <NavBrand>
      <Link to="/" style={{ borderBottom: 'none' }}>
        {title}
      </Link>
    </NavBrand>
  );

  const navbar = (
    <Container className="max-width">
      <Navbar>
        {logo}
        <Nav>
          <NavItemLink href="/" aria-current="page">
            home
          </NavItemLink>
          <NavItemLink href="/projects">Projects</NavItemLink>
          <NavItemLink href="/blog">Blog</NavItemLink>
          <NavItemLink href="/about">About</NavItemLink>
        </Nav>
      </Navbar>
    </Container>
  );

  const signature = (
    <div>
      Built with <Emoji label="love" symbol="❤️" /> &{' '}
      <a href="https://www.gatsbyjs.org">Gatsby</a>.
    </div>
  );
  const hostedOnGitlab = (
    <div>
      Source code hosted on{' '}
      <a href="https://gitlab.com/flrnd/flrnd.gitlab.io">Gitlab</a>.
    </div>
  );

  return (
    <Page>
      <GlobalStyle />
      <Header id="#top">{navbar}</Header>
      <Content className="max-width">{children}</Content>
      <Footer>
        <section>{signature}</section>
        <section>{hostedOnGitlab}</section>
        <section>
          <ListSocialIcons />
        </section>
      </Footer>
    </Page>
  );
};

export default Layout;
