/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { useSiteMetadata } from '../hooks/use-site-metadata';

function SEO({ seoDescription, lang, meta, seoKeywords, seoTitle }) {
  const { author, title, description, siteUrl } = useSiteMetadata();
  const metaDescription = seoDescription || description;

  return (
    <Helmet
      htmlAttributes={{
        lang,
      }}
      title={title}
      titleTemplate={`%s | ${seoTitle || title}`}
      meta={[
        {
          name: `description`,
          content: metaDescription,
        },
        {
          property: `og:title`,
          content: title,
        },
        {
          property: `og:description`,
          content: metaDescription,
        },
        {
          property: `og:type`,
          content: `website`,
        },
        {
          property: `og:url`,
          content: `${siteUrl}`,
        },
        {
          name: `twitter:card`,
          content: `summary`,
        },
        {
          name: `twitter:creator`,
          content: author,
        },
        {
          name: `twitter:title`,
          content: title,
        },
        {
          name: `twitter:description`,
          content: metaDescription,
        },
      ]
        .concat(
          seoKeywords.length > 0
            ? {
                name: `keywords`,
                content: seoKeywords.join(`, `),
              }
            : []
        )
        .concat(meta)}
    />
  );
}

SEO.defaultProps = {
  lang: `en`,
  meta: [],
  seoKeywords: [
    `blog`,
    `gatsby`,
    `gatsbyjs`,
    `react`,
    `design`,
    `designer`,
    `developer`,
    `development`,
    `javascript`,
    `nodejs`,
    `open source`,
    `florian rand`,
  ],
  seoDescription: `Florian Rand is a multidisciplinary designer and software developer passionate with technology, open source and design.`,
};

SEO.propTypes = {
  seoDescription: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.arrayOf(PropTypes.object),
  seoKeywords: PropTypes.arrayOf(PropTypes.string),
  seoTitle: PropTypes.string.isRequired,
};

export default SEO;
