import React from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';
import Emoji from './emoji';

const Post = styled.section`
  padding-top: 2em;
  padding-bottom: 4em;
  position: relative;
`;
const BlogEntry = props => {
  const { date, title, timeToRead, slug } = props;
  return (
    <Post key={slug}>
      <div style={{ fontSize: '18px' }}>
        <time dateTime={date}>{date}</time>{' '}
        <Emoji label="Time to read" symbol="📖" /> {timeToRead} min read
      </div>
      <Link to={slug}>
        <h2 className="heading">
          <span>{title}</span>
        </h2>
      </Link>
    </Post>
  );
};

export default BlogEntry;
