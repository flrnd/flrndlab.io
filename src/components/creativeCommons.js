import React from 'react';

const CreativeCommons = () => (
  <a
    rel="license"
    href="http://creativecommons.org/licenses/by-sa/4.0/"
    style={{ fontSize: '12px' }}
  >
    The content of this site is licensed under Creative Commons
    Attribution-ShareAlike 4.0 International License
  </a>
);

export default CreativeCommons;
