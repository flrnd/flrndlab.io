import React from 'react';
import { List, ListItemLink } from './design/list';

const ListSocialIcons = () => (
  <List style={{ padding: '2.2em' }}>
    <ListItemLink href="https://github.com/flrnd" label="GitHub profile">
      GitHub
    </ListItemLink>
    <ListItemLink href="https://gitlab.com/flrnd" label="GitLab profile">
      GitLab
    </ListItemLink>
    <ListItemLink href="https://dev.to/flrnd" label="dev.to profile">
      DEV
    </ListItemLink>
    <ListItemLink
      href="https://www.linkedin.com/in/flrnd/"
      label="Linkedin profile"
    >
      Linkedin
    </ListItemLink>
    <ListItemLink href="https://twitter.com/flrnprz" label="Twitter profile">
      twitter
    </ListItemLink>
  </List>
);

export default ListSocialIcons;
