import React from 'react';
import Button from './design/button';
import { FaArrowUp } from 'react-icons/fa';

const BackToTop = () => (
  <a href="#top" className="d-flex justify-content-center btn">
    <Button type="button" color="primary" a11yTitle="Back to top">
      Back to top <FaArrowUp />
    </Button>
  </a>
);

export default BackToTop;
