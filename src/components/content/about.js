import React from 'react';
import { Link } from 'gatsby';
import Emoji from '../emoji';

const AboutContent = () => (
  <>
    <h1 className="title">About</h1>
    <p>
      I'm a multidisciplinary designer and software developer who also likes to
      write from time to time, animals (specially cats), skateboarding, sci-fi
      novels and music. I really do love music.{' '}
      <Emoji label="nerdy-smile" symbol="🤓" />.
    </p>
    <h2 className="title">biography</h2>
    <p>Born in Granada, Spain.</p>
    <p>
      My first programming steps started when I was a child with an 8-bit MSX,
      but nothing serious. I'm not one of those geniuses. Just a child with nine
      years old who learned how to program in BASIC and some z80 ASM to cheat
      some games.
    </p>
    <p>
      I always had a thing for technology and computers. Thanks to one of my
      best friends I'm a proud Linux user since 1996. But, I wasn't attracted
      only to technology. I also loved everything hand-crafted. I was that
      distracted kid in school and highschool doodling in a piece of paper.
    </p>
    <p>
      I've done a lot in my professional career. I moved to Barcelona where I
      worked as a freelance retoucher, fashion photographer, CGI generalist,
      email marketing developer, web designer, web developer and Art director.
      I've worked for small advertising agencies and big ones. I've worked for
      big brands like Honda, Seat, Equivalenza, Tarik Ediz, TBS, Lidl... But the
      work I'm most proud of is from my smallest clients.
    </p>
    <p>
      In 2016 for personal reasons, I started a career change focusing more on
      the technical side and my nerd roots. You can read more about this in
      detail in my{' '}
      <Link to="/blog/from-graphicdesign-to-development/">
        from graphic design to development
      </Link>{' '}
      article.
    </p>
  </>
);

export default AboutContent;
