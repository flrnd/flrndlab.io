import React from 'react';
import { StaticQuery, graphql } from 'gatsby';
import Helmet from 'react-helmet';

export default () => (
  <StaticQuery
    query={graphql`
      query helmetQuery {
        site {
          siteMetadata {
            title
            author
            description
            keywords
          }
        }
      }
    `}
    render={data => {
      const { description, keywords, title, author } = data.site.siteMetadata;
      return (
        <Helmet>
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"
          />
          <meta name="description" content={description} />
          <meta name="keywords" content={keywords} />
          <title>{title}</title>
          <html lang="en" />
          {/* Google / Search Engine Meta Tags */}
          <meta itemprop="name" content={author} />
          <meta itemprop="description" content={description} />
        </Helmet>
      );
    }}
  />
);
