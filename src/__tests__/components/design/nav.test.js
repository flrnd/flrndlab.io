import React from 'react';
import renderer from 'react-test-renderer';
import {
  Nav,
  Navbar,
  NavItem,
  NavBrand,
  NavItemLink,
} from '../../../components/design/nav';

describe('Navbar snapshot', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <Navbar>
          <NavBrand>Logo</NavBrand>
          <Nav>
            <NavItemLink href="/" aria-current="page">
              page
            </NavItemLink>
            <NavItemLink href="/other">other</NavItemLink>
            <NavItem>Some item</NavItem>
          </Nav>
        </Navbar>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});

