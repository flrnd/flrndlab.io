import React from 'react';
import renderer from 'react-test-renderer';

import { ListItem, ListItemLink, List } from '../../../components/design/list';

describe('List snapshot', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <List>
          <ListItem>Item1</ListItem>
          <ListItemLink href="/" label="home">
            home
          </ListItemLink>
        </List>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
