import React from 'react';
import renderer from 'react-test-renderer';
import Button from '../../../components/design/button';

describe('Button', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <Button
          type="button"
          a11Title="this is a button"
          fontSize="12px"
          fontWeight="400"
          disabled
        >
          some button
        </Button>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
