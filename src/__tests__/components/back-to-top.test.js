import React from 'react';
import renderer from 'react-test-renderer';
import BackToTop from '../../components/back-to-top';

describe('Emoji', () => {
  it('renders correctly', () => {
    const tree = renderer.create(<BackToTop />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
