import React from 'react';
import renderer from 'react-test-renderer';
import ListSocialIcons from '../../components/socialIcons';

describe('Social Icons List', () => {
  it('renders correctly', () => {
    const tree = renderer.create(<ListSocialIcons />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
