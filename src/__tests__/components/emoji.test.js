import React from 'react';
import renderer from 'react-test-renderer';
import Emoji from '../../components/emoji';
import { render } from '@testing-library/react';

describe('Emoji', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(<Emoji label="copyright" symbol="©️" />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders aria-label', () => {
    const SomeEmoji = () => <Emoji label="Label" symbol="c" />;
    const { getByRole } = render(<SomeEmoji />);
    expect(getByRole('img')).toHaveAttribute('class', 'emoji');
    expect(getByRole('img')).toHaveTextContent('c');
    expect(getByRole('img')).toHaveAttribute('aria-label');
    expect(getByRole('img')).toHaveAttribute('aria-hidden');
  });
});
