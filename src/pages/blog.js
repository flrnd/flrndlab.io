import React from 'react';
import Layout from '../components/layout';
import SEO from '../components/seo';
import BlogEntry from '../components/blog-entry';
import { Container } from '../components/design/layout';
import seoPages from '../util/seoPages';
import BackToTop from '../components/back-to-top';
import { useSiteMetadata } from '../hooks/use-site-metadata';
import { useAllMarkdownRemark } from '../hooks/use-all-markdown-remark';

const BlogIndex = props => {
  const { title } = useSiteMetadata();
  const posts = useAllMarkdownRemark();
  const location = props.location;
  const { seoTitle, seoDescription, seoKeywords } = seoPages.blog;

  return (
    <Layout location={location} title={title}>
      <SEO
        seoTitle={seoTitle}
        seoDescription={seoDescription}
        seoKeywords={seoKeywords}
      />
      <Container className="text-content">
        <h1 className="small">All posts</h1>
        {posts.edges.map(({ node }) => {
          const title = node.frontmatter.title || node.fields.slug;
          const { date } = node.frontmatter;
          const slug = node.fields.slug;
          const timeToRead = node.timeToRead;

          return (
            <BlogEntry
              title={title}
              date={date}
              slug={slug}
              timeToRead={timeToRead}
              key={slug}
            />
          );
        })}
        <BackToTop />
      </Container>
    </Layout>
  );
};

export default BlogIndex;
