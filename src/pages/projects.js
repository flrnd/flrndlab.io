import React from 'react';
import Layout from '../components/layout';
import SEO from '../components/seo';
import { Container, Grid } from '../components/design/layout';
import Card from '../components/design/card';
import seoPages from '../util/seoPages';
import BackToTop from '../components/back-to-top';
import { useSiteMetadata } from '../hooks/use-site-metadata';
import { useAllProjectsMarkdownRemark } from '../hooks/use-all-projects-markdown-remark';

const ProjectsIndex = props => {
  const { title } = useSiteMetadata();
  const projects = useAllProjectsMarkdownRemark();
  const location = props.location;
  const { seoTitle, seoDescription, seoKeywords } = seoPages.projects;

  return (
    <Layout location={location} title={title}>
      <SEO
        seoTitle={seoTitle}
        seoDescription={seoDescription}
        seoKeywords={seoKeywords}
      />
      <Container>
        <h1 className="small">All projects</h1>
        <Grid>
          {projects.edges.map(({ node }) => {
            const title = node.frontmatter.title || node.fields.slug;
            const { url, category, description, keywords } = node.frontmatter;
            const slug = node.fields.slug;

            return (
              <Card
                key={slug}
                title={title}
                description={description}
                url={url}
                category={category}
                keywords={keywords}
              />
            );
          })}
        </Grid>
        <BackToTop />
      </Container>
    </Layout>
  );
};

export default ProjectsIndex;
