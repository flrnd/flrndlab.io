import React from 'react';
import Layout from '../components/layout';
import SEO from '../components/seo';
import { Container } from '../components/design/layout';
import seoPages from '../util/seoPages';
import { useSiteMetadata } from '../hooks/use-site-metadata';

const Index = props => {
  const location = props.location;
  const { title } = useSiteMetadata();
  const { seoTitle, seoDescription, seoKeywords } = seoPages.index;
  return (
    <Layout location={location} title={title}>
      <SEO
        seoTitle={seoTitle}
        seoDescription={seoDescription}
        seoKeywords={seoKeywords}
      />
      <Container>
        <h1 className="hero">Florian Rand</h1>
        <p className="p-hero">
          Multidisciplinary designer, software developer and a very bad writer.
        </p>
      </Container>
    </Layout>
  );
};

export default Index;
