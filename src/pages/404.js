import React from 'react';
import Layout from '../components/layout';
import SEO from '../components/seo';
import { useSiteMetadata } from '../hooks/use-site-metadata';

const NotFoundPage = props => {
  const { location } = props;
  const { title } = useSiteMetadata();
  return (
    <Layout location={location} title={title}>
      <SEO seoTitle="404: Not Found" />
      <h1>404</h1>
      <h3>This is not the page you are looking for</h3>
    </Layout>
  );
};

export default NotFoundPage;
