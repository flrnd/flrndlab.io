import React from 'react';
import Layout from '../components/layout';
import SEO from '../components/seo';
import { Container } from '../components/design/layout';
import AboutContent from '../components/content/about';
import seoPages from '../util/seoPages';
import BackToTop from '../components/back-to-top';
import { useSiteMetadata } from '../hooks/use-site-metadata';

const About = props => {
  const { title } = useSiteMetadata();
  const { seoTitle, seoDescription, seoKeywords } = seoPages.about;

  return (
    <Layout location={props.location} title={title}>
      <SEO
        seoTitle={seoTitle}
        seoDescription={seoDescription}
        seoKeywords={seoKeywords}
      />
      <Container className="text-content">
        <AboutContent />
        <BackToTop />
      </Container>
    </Layout>
  );
};

export default About;
